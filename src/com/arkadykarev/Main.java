package com.arkadykarev;

import com.arkadykarev.list.ListItemsRemoveHelper;
import com.arkadykarev.list.ListSequencesRemoveHelper;
import com.arkadykarev.tree.BinaryTreeNode;
import com.arkadykarev.tree.BinaryTreeTraverseHelper;

import java.util.*;

public class Main {

	private final static int MIN_NUMBER_OF_DUPLICATE_VALUES = 3;
	
    public static void main(String[] args) {
		println("====== TEST 1 ======");
		println("Searching max tree height..");
		testTree();

		println("\n====== TEST 2 ======");
		println("Removing matches from Lists..");
		testLists();
	}

	private static void testTree() {
		BinaryTreeNode tree = new BinaryTreeNode(
				new BinaryTreeNode(new BinaryTreeNode(new BinaryTreeNode(), null), null), 
				new BinaryTreeNode(new BinaryTreeNode(), null)
		);
		println("Maximum tree height from root: " + BinaryTreeTraverseHelper.calculateHeightOfTree(tree));
	}

	private static void testLists() {
		Integer[] initArray = {1,1,1,2,2,3,4,5,5,5,5,2,5,6,7,8,1,1,9,10,0,0,0,7};
		List<Integer> itemsList;
		println("Minimum number of same elements for deleting from Lists: " + MIN_NUMBER_OF_DUPLICATE_VALUES);

		itemsList = new ArrayList<>(Arrays.asList(initArray));
		println("\nArrayList before removing same elements:");
		println(itemsList);
		ListItemsRemoveHelper.removeSameItems(itemsList, MIN_NUMBER_OF_DUPLICATE_VALUES);
		println("ArrayList after processing:");
		println(itemsList);

		itemsList = new LinkedList<>(Arrays.asList(initArray));
		println("\nLinkedList before removing same elements:");
		println(itemsList);
		ListItemsRemoveHelper.removeSameItems(itemsList, MIN_NUMBER_OF_DUPLICATE_VALUES);
		println("LinkedList after processing:");
		println(itemsList);

		itemsList = new ArrayList<>(Arrays.asList(initArray));
		println("\nArrayList before removing sequences of same elements:");
		println(itemsList);
		ListSequencesRemoveHelper.removeSameItemSequences(itemsList, MIN_NUMBER_OF_DUPLICATE_VALUES);
		println("ArrayList after processing:");
		println(itemsList);

		itemsList = new LinkedList<>(Arrays.asList(initArray));
		println("\nLinkedList before removing sequences of same elements:");
		println(itemsList);
		ListSequencesRemoveHelper.removeSameItemSequences(itemsList, MIN_NUMBER_OF_DUPLICATE_VALUES);
		println("LinkedList after processing:");
		println(itemsList);
	}

	private static void println(Object o) {
		System.out.println(o.toString());
	}
}