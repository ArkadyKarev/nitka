package com.arkadykarev.list;


import java.util.*;

/**
 * Contains utility methods for removing elements from List.
 * 
 * Created by arkady on 02/02/15.
 */
public class ListItemsRemoveHelper {

	/**
	 * Removes all elements with same values, which included in list n or more times
	 * 
	 * @param items
	 * @param minElementsOccurrence
	 * @return modified list
	 */
	public static List<Integer> removeSameItems(List<Integer> items, int minElementsOccurrence) {
		if (minElementsOccurrence <= 0 || items == null || items.size() < minElementsOccurrence ) return items;
		
		return items instanceof LinkedList ? 
				simpleRemove(items, minElementsOccurrence) : 
				advancedRemove(items, minElementsOccurrence);
	}

	private static List<Integer> simpleRemove(List<Integer> items, int minElementsOccurrence) {
		Set<Integer> itemsToRemove = new HashSet<>();

		for (int i = items.size(); --i >= 0;) {
			Integer item = items.get(i);
			if (isItemHasToBeRemoved(items, item, itemsToRemove, minElementsOccurrence)) {
				items.remove(i);
			}
		}
		return items;
	}
	
	private static List<Integer> advancedRemove(List<Integer> items, int minElementsOccurrence) {
		Set<Integer> itemsToRemove = new HashSet<>();
		
		for (int i = 0; i < items.size(); i++) {
			while (isItemHasToBeRemoved(items, items.get(i), itemsToRemove, minElementsOccurrence)) {
				Integer detachedItem = removeItemFromTail(items, i, itemsToRemove, minElementsOccurrence);
				if (detachedItem == null) {
					items.remove(i);
					break;
				}
				items.set(i, detachedItem);
			}
		}
		return items;
	}

	private static boolean isItemHasToBeRemoved(List<Integer> items, Integer item, Set<Integer> itemsToRemove, int sequenceSize) {
		if (itemsToRemove.contains(item)) return true;

		if (Collections.frequency(items, item) >= sequenceSize) {
			itemsToRemove.add(item);
			return true;
		}
		return false;
	}

	private static Integer removeItemFromTail(List<Integer> items, int lowerIndex, Set<Integer> itemsToRemove, int sequenceSize) {
		for (int i = items.size(); --i > lowerIndex;) {
			Integer item = items.get(i);
			items.remove(i);
			if (!isItemHasToBeRemoved(items, item, itemsToRemove, sequenceSize)) {
				return item;
			}
		}
		return null;
	}
}