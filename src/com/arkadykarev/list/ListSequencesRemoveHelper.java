package com.arkadykarev.list;

import java.util.LinkedList;
import java.util.List;

/**
 * Contains utility methods for removing sequences of elements from List.
 * 
 * Created by arkady on 02/02/15.
 */
public class ListSequencesRemoveHelper {

	/**
	 * Removes sequences of the elements with same values
	 *
	 * @param items
	 * @param minSequenceLength
	 * @return modified list
	 */
	public static List<Integer> removeSameItemSequences(List<Integer> items, int minSequenceLength) {
		if (minSequenceLength <= 0 || items == null || items.size() < minSequenceLength ) return items;

		for (int i = items.size(); --i >= 0;) {
			int sequenceSize = calculateSequenceLength(items, i);
			if (sequenceSize >= minSequenceLength) {				
				if (items instanceof LinkedList) {
					for (int j = 0; j < sequenceSize; j++) {
						items.remove(i--);
					}
				} else {
					int firstIndexOfSequence = i - sequenceSize + 1; //+1 to include this element into sequence
					items.subList(firstIndexOfSequence, i + 1).clear();
					i = i - sequenceSize;
				}
			}
		}
		return items;
	}

	private static int calculateSequenceLength(List<Integer> items, int lastIndex) {
		Integer initValue = items.get(lastIndex);
		int sequenceLength = 1;
		while (lastIndex - sequenceLength >= 0 && items.get(lastIndex - sequenceLength).equals(initValue)) {
			sequenceLength++;
		}
		return sequenceLength;
	}
}
