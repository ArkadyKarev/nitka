package com.arkadykarev.tree;

/**
 * Contains utility methods for traversing binary tree.
 * 
 * Created by arkady on 02/02/15.
 */
public class BinaryTreeTraverseHelper {

	/**
	 * Calculates height of binary tree from given node
	 * 
	 * @param node
	 * @return
	 */
	public static int calculateHeightOfTree(BinaryTreeNode node) {
		if (node == null) {
			return 0;
		} else {
			return 1 + Math.max(calculateHeightOfTree(node.getLeft()), calculateHeightOfTree(node.getRight()));
		}
	}
}
