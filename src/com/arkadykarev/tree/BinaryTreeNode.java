package com.arkadykarev.tree;

/**
 * Represents node of binary tree
 * 
 * Created by arkady on 02/02/15.
 */
public class BinaryTreeNode {

	private BinaryTreeNode left;
	private BinaryTreeNode right;

	public BinaryTreeNode() {
	}

	public BinaryTreeNode(BinaryTreeNode left, BinaryTreeNode right) {
		this.left = left;
		this.right = right;
	}

	public BinaryTreeNode getLeft() {
		return left;
	}

	public void setLeft(BinaryTreeNode left) {
		this.left = left;
	}

	public BinaryTreeNode getRight() {
		return right;
	}

	public void setRight(BinaryTreeNode right) {
		this.right = right;
	}
}
